# -*- coding: utf-8 -*-

"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (QgsProcessing,
                       QgsFeatureSink,
                       QgsProcessingException,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterFeatureSink,
                       QgsProcessingParameterField,
                       QgsProcessingParameterString,
                       QgsProcessingParameterVectorLayer,
                       QgsProcessingParameterFile,
                       QgsField,
                       QgsExpression,
                       QgsExpressionContext,
                       QgsExpressionContextUtils,
                       QgsSymbol,
                       QgsRendererRange,
                       QgsProcessingParameterNumber,
                       QgsGraduatedSymbolRenderer

                       )
from qgis import processing


class ProccesBufferSymbologie(QgsProcessingAlgorithm):
    INPUT_1 = 'INPUT_1'             # variable de la couche en entree pour le buffer
    INPUT_BUFFERDIST = 'BUFFERDIST' # variable de la distance du buffer
    OUTPUT_BUFFER = 'OUTPUT_BUFFER' # variable de la sortie du buffer


    INPUT_2 = 'INPUT_2'                        #variable de la couche d entree pour la selection
    CAPACITE ='CAPACITE'                       #Champ pour filter la capacite des cable
    MODE_POSE = "MODE_POSE"                    #Champ pour filtrer le mode de pose des cable
    OUTPUT_PRISES = 'OUTPUT_PRISES'            #variable de la sortie de la carte symbologie

    def tr(self, string):
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        return ProccesBufferSymbologie()

    def name(self):
        return 'Buffer'

    def displayName(self):
        return self.tr('Buffer')

    def group(self):
        return self.tr('Buffer')

    def groupId(self):
        return 'Buffer'

    def shortHelpString(self):

        return self.tr("Ce plugin permet la création automatique d'une carte avec un buffer autour des cables et une symbologie graduée en fonction du nombre de prises.")

    def initAlgorithm(self, config=None):

#Paramètre de l'interface 
#entrer de la couche boite
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT_1, "Couche boites")) #selection de la 1ère couche

#affectation de la distance du buffer
        self.addParameter(
            QgsProcessingParameterNumber(
                self.INPUT_BUFFERDIST, "Distance du buffer",
                QgsProcessingParameterNumber.Double,
                5.0))



#Selection de la capacite et le mode de pose

        self.addParameter(
            QgsProcessingParameterVectorLayer(
                self.INPUT_2,
                self.tr('Couche cables)'),
                [QgsProcessing.TypeVectorAnyGeometry]
            ))

#Permet à l'utilisateur de renseigner la capacité des cables
        self.addParameter(QgsProcessingParameterNumber(
            self.CAPACITE, "Capacité",
            QgsProcessingParameterNumber.Integer
            ))

 # permet a l'utilisateur de renseigner le mode de pose des cable (texte , voir le type de pose)
        self.addParameter(QgsProcessingParameterString
            (self.MODE_POSE,
            'Mode de pose',
            optional=False,
            defaultValue=None))

        #recupere la sortie du resultat final pour les buffers
        self.addParameter(
            QgsProcessingParameterFeatureSink(
                self.OUTPUT_BUFFER, "Output buffer (boites)"))

        #recupere la sortie du resultat final pour la carte symbologie par nombre de prise
        self.addParameter(
            QgsProcessingParameterFeatureSink(
                self.OUTPUT_PRISES,
                self.tr('Output de la symbologie (cables)')
            )
        )


    def processAlgorithm(self, parameters, context, feedback):

#executer le processing du buffer
        bufferresult = processing.run('native:buffer',
            {'INPUT': parameters[self.INPUT_1],
            'DISTANCE': parameters[self.INPUT_BUFFERDIST],
            'SEGMENTS': 5,
            'END_CAP_STYLE': 0,
            'JOIN_STYLE': 0,
            'MITER_LIMIT': 10,
            'DISSOLVE': True,
            'OUTPUT': parameters[self.OUTPUT_BUFFER]},
            context=context, feedback=feedback, is_child_algorithm=True)
        buffered = bufferresult['OUTPUT']




#Selection de la capacité et le mode de pose
		# on recupere la couche selectionner
        select = self.parameterAsVectorLayer(
            parameters,
            self.INPUT_2,
            context
        )

		# on applique le filtre sélectionner la capacite superieur ou egale à entree, et le mode de pose , qu'on a recuprer des champ à remplir
        select.selectByExpression(" \"capacite\" >= {} AND \"mode_pose\" LIKE '%{}%' ".format(parameters[self.CAPACITE], parameters[self.MODE_POSE] ) )



        # sauvegarder la selection dans la deuxieme sortie
        capacite_select=processing.run('qgis:saveselectedfeatures',
        {'INPUT':select,
        'OUTPUT':parameters[self.OUTPUT_PRISES]},
        context=context, feedback=feedback, is_child_algorithm=True)
        mescables=capacite_select['OUTPUT']


#Representer le nombre de prises de la selection

#recuprer la selection par le getmaplayer
        mescables=context.getMapLayer(mescables)

        #Symbiologie gradué par le nombre de prises
        nbr_prises = (
            ('Faible', 1, 9, 'green'),
            ('Moyen_1', 10, 25, 'yellow'),
            ('Moyen_2', 26, 100, 'orange'),
            ('Elevé', 100, 1000000, 'red'),

        )

        # creation de la symbologie
        ranges = []
        for label, lower, upper, color in nbr_prises:
            symbol = QgsSymbol.defaultSymbol(mescables.geometryType())
            symbol.setColor(QColor(color))
            rng = QgsRendererRange(lower, upper, symbol, label)
            ranges.append(rng)

        # creer la carte avec la symbologie sur la couche designées
        expression = 'nb_prises' # le nom du champ cible
        renderer = QgsGraduatedSymbolRenderer(expression, ranges) #creer la garduation
        mescables.setRenderer(renderer) #affecter la symbologie a la couche semectionner

        return {self.OUTPUT_PRISES: mescables} #on retourn la couche avec la symbologie demandée
